<?php

$_texts = [
    'header.keywords' => 'pinute, bière, pinte, alcool, temps, durée',
    'header.description' => 'La pinute, temps pris pour boire une pinte... Mais pas que..',
    'header.title' => 'La pinute',
    'title' => 'La pinute',
    'subtitle' => 'Ne perdez plus votre temps. Buvez le !',
    'services.title' => 'Définition',
    'services.subtitle' => 'Temps que l\'on prend pour boire une pinte. <br />La pinute représentant approximativement 30 minutes.',
    'services.parts' => [[
        'icon' => 'lnr-cog',
        'title' => 'Modulable',
        'description' => 'Ne se limitant pas à une pinte, elle est adaptable à toutes boissons alcoolisées.'
    ], [
        'icon' => 'lnr-clock',
        'title' => 'Adaptable',
        'description' => 'Aucune durée fixe. La pinute s\'adapte à son environnment. La météo, l\'ambiance, l\'envie. Tout est pris en compte de manière simple.'
    ], [
        'icon' => 'lnr-thumbs-up',
        'title' => 'Efficacité',
        'description' => 'Permet de fixer des rendez-vous avec une heure d\'arrivée souple. Fini les attentes interminables...'
    ]],
    'features.title' => 'Ça marche',
    'features.subtitle' => 'La pinute peut être utile dans de nombreuses situations.<br />Vous trouverez ci-dessous une liste non exhaustive.',
    'features.parts' => [[
        'icon' => 'lnr-store',
        'title' => 'Au bar',
        'description' => 'T\'as bien le temps de reprendre une pinute ?!'
    ], [
        'icon' => 'lnr-train',
        'title' => 'Dans les transports',
        'description' => 'Suite à un malaise voyageur sur les voies votre train 5 pinutes de retard.'
    ], [
        'icon' => 'lnr-heart',
        'title' => 'En couple',
        'description' => '"Chérie, j\'suis au bar avec des potes là. Je serai là dans 2 pinutes."'
    ], [
        'icon' => 'lnr-mustache',
        'title' => 'Au travail',
        'description' => 'Excuse-moi, tu aurais une pinute à m\'accorder pour qu\'on parle du dossier mustache.'
    ]],
    'video.title' => 'Pas encore convaincu ?',
    'video.description' => 'Voici un documentaire canadien qui retrace les origines de la bière et son indéniable apport à l\'humanité.',
    'video.link' => 'https://www.dailymotion.com/embed/video/xyst0f?autoplay=true',
    'testimonials' => [[
        'image' => 'couple.jpg',
        'testimonial' => 'La pinute a sauvé notre couple.<br />Avant, on se fixait des horaires et il était toujours en retard.<br /> Maintenant, fini les attentes interminables.',
        'name' => 'Michelle Flash',
        'description' => 'Pinuteuse de la première heure'
    ], [
        'image' => 'beer-transport.jpg',
        'testimonial' => 'La pinute est très abordable et très facile à appliquer au quotidien.<br /> Cela a vraiment bouleversé mon rapport au temps.',
        'name' => 'Jean Bois',
        'description' => 'Pinuteur convaincu'
    ], [
        'image' => 'hangover.jpg',
        'testimonial' => 'Attention l\'abus de pinute est dangereux pour la santé.<br />Surtout si c\'est de la Chouffe...',
        'name' => 'Julien de La Bath',
        'description' => 'Pinuteur émérite'
    ], [
        'image' => 'beer-girl.jpg',
        'testimonial' => 'J\'essaie toujours de prendre une pinute ou deux pour me détendre après le boulot.',
        'name' => 'Lucienne Enquille',
        'description' => 'Pinuteuse occasionnelle'
    ], [
        'image' => 'call.jpg',
        'testimonial' => 'Chérie, j\'suis au bar j\'arrive dans une pinute.',
        'name' => 'Pierre Planteur',
        'description' => 'Mec en couple'
    ], [
        'image' => 'heineken.jpg',
        'testimonial' => 'La Heineken, c\'est vraiment de la merde...',
        'name' => 'Pierre Paul Jacques',
        'description' => 'Président du comité de lutte contre la bière de merde.'
    ], [
        'image' => 'beach-beer.jpg',
        'testimonial' => 'Sur la plage on s\'ennuie vite...<br />Heureusement, le temps passe plus vite en pinutant.',
        'name' => 'Loïc Plagiste',
        'description' => 'Chomeur en vacances'
    ], [
        'image' => 'triste.jpg',
        'testimonial' => 'Mouais, bof pas terrible comme concept.',
        'name' => 'Marie-Gabriel du Presbytière',
        'description' => 'Buveur d\'eau'
    ], [
        'image' => 'cider-boy.jpg',
        'testimonial' => 'Perso, j\'bois que du cidre... Mais ça marche aussi.<br /> Le plus dingue c\'est que j\'appliquais ce concept sans même le savoir.<br /> C\'est cool de pouvoir lui donner un nom.',
        'name' => 'Erwan du Sistr',
        'description' => 'buveur de cidre'
    ], [
        'image' => 'palma.jpeg',
        'testimonial' => 'J\'aime la pinute.<br />J\'aime la pinute fraiche, j\'aime la pinute chaude. J\'aime quand elle est âpre, quand elle est aigre, quand elle est acide.<br />Et surtout j\'aime le piment d\'espelette.',
        'name' => 'Yves Cote de Porc',
        'description' => 'buveur de cidre'
    ]]
];

shuffle($_texts['testimonials']);

$_menu = [
    'hero-area' => 'Home',
    'services' => 'Définiton',
    'features' => 'Ça marche',
    // 'portfolios' => 'Works',
    // 'pricing' => 'Pricing',
    // 'team' => 'Team'
];

include('view.php');
